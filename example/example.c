///< this is just a example source file for cmake
#include "config.h"

#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    printf("Hello, I'm component.\n");
    printf("component name: %s\n", COMPONENT_NAME);
    printf("component hexVerson: %08X\n", COMPONENT_VERSION);
    printf("component strVerson: %s\n", COMPONENT_VERSION_STR);
    printf("component version description: %s\n", COMPONENT_VERSION_DESCR);

    return 0;
}
