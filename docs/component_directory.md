# 组件制品目录结构规范 #

## 一、组件制品目录结构规范

针对组件，构建的制品包内部结构需要满足如下目录结构：

```
/docs              # 存放设计文档、接口文档、使用手册等【必选】
    /$component1   # 组件1文档
    /$component2   # 组件2文档
/include           # 存放组件库二次开发的头文件【必选】
    /$component1   # 组件1头文件
    /$component2   # 组件2头文件
/example           # 存放组件实例程序【必选】
    /$component1   # 组件1示例程序
    /$component2   # 组件2示例程序
/lib               # 存放组件构建生成的静态库与动态库【必选】
/tools             # 存放组件提供的工具可执行程序【可选】
/config            # 存放组件配置文件【可选】
    /$component1   # 组件1配置文件示例
    /$component2   # 组件2配置文件示例
```

- **组件库二次开发方法**：组件在做二次开发时，将多个组件解压到同一个目录即可
    - 其中docs、include、example和config目录下按组件名划分二级目录结构，lib和tools为一级目录结构；
    - 头文件依赖根目录为include，包含头文件的方法为：#include "moduleName/moduleHead.h"；
    - 库文件依赖根目录为lib，所有库均存放在此目录，链接方法为：-Llib -lxxx；
- **组件库产品集成方法**：组件库在做集成时，为了减少空间占用，可以删除无用的文件，集成步骤为：
    - 将产品用到的所有组件解压到同一个目录下；
    - 删除docs、include、example和config目录；
    - 删除lib目录下所有lib*.a文件。

例如三方库libev的目录结构如下：

```
libev-V4.23.0.0$ tree .
.
├── docs
│   └── libev
│       └── libev_manual.md
├── example
│   └── libev
│       ├── echo_client.cc
│       ├── echo_server.cc
│       ├── ev_async.c
│       ├── ev_child.c
│       ├── ev_cleanup.c
│       ├── ev_fork1.c
│       ├── ev_fork.c
│       ├── ev_idle.c
│       ├── ev_io.c
│       ├── ev_periodic.c
│       ├── ev_prepare_check.c
│       ├── ev_signal.c
│       ├── ev_stat.c
│       └── ev_timer.c
├── include
│   └── libev
│       ├── event.h
│       ├── ev++.h
│       └── ev.h
└── lib
    ├── libev.a
    ├── libev.la
    ├── libev.so -> libev.so.4.0.0
    ├── libev.so.4 -> libev.so.4.0.0
    └── libev.so.4.0.0
 
7 directories, 23 files
```

## 二、制品命名规范

构建发布的二进制包需要满足《固件命名规范》，详细规则如下：

- 组件：<moduleName>-<Vx.x.x.x>-<buildTime><.platform><.Debug/Release>.tar.gz
- 服务：<serviceName>-<Vx.x.x.x>-<buildTime><.platform><.Debug/Release>.tar.gz

比如libev的三方组件，针对不同平台构建后的二进制包名为：

```
libev-V4.33.0-20230404203414.LC1881.Debug.tar.gz
libev-V4.33.0-20230404203414.SM8475.Debug.tar.gz
libev-V4.33.0-20230404203414.SDM660.Debug.tar.gz
libev-V4.33.0-20230404203414.LC1881.Release.tar.gz
libev-V4.33.0-20230404203414.SM8475.Release.tar.gz
libev-V4.33.0-20230404203414.SDM660.Release.tar.gz
```
