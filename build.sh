#!/bin/bash

CURDIR=$(cd $(dirname $0); pwd)
if [ ! -f ./.git/hooks/commit-msg ]; then
    cp ./tools/commit-msg ./.git/hooks/commit-msg
    chmod +x ./.git/hooks/commit-msg
fi
if [ ! -f ./.git/hooks/prepare-commit-msg ]; then
    cp ./tools/prepare-commit-msg ./.git/hooks/prepare-commit-msg
    chmod +x ./.git/hooks/prepare-commit-msg
fi

function helpMessage(){
    echo "SYNOPSIS"
    echo "    ./build.sh -p <platform> -t <Debug | Release> -v [build_version]"
    echo "OPTIONS"
    echo "    -h: show help for build.sh"
    echo "    -P: package the distribution files(install) for publication"
    echo "    -p: specified platform, like X86, MINI2440 ..."
    echo "    -t: specified build type, alternative Debug and Release"
    echo "    -v: build version, it's an integer data"
    echo "    -c: clean the temporary build files"
    echo "    -b: just build, print all build information for debug"
    echo "    -d: only delete build files and release files"
    echo "    -D: delete all files(build, release), and deinit submodules"
}

while getopts "bcDdhPp:t:v:" opt; do
    case $opt in
    P)
        BUILD_PACK=true
        ;;
    p)
        SOCPLAT="$OPTARG"
        toolchain="${CROSS_COMPILE_DIR}/$OPTARG.cmake"
        ;;
    t)
        if [ "$OPTARG" != "Debug" ] && [ "$OPTARG" != "Release" ]; then
            echo "build type only alternative debug and release"
            exit 1
        fi
        BUILD_TYPE="$OPTARG"
        ;;
    v)
        BUILD_VER="$OPTARG"
        ;;
    h)
        helpMessage
        exit 0
        ;;
    b)
        if [ -d output/build ]; then
            cd output/build && make VERBOSE=1
            exit 0
        else
            BUILD_DETAIL=true
        fi
        ;;
    c)
        if [ ! -d output/build ]; then
            echo "Need build target first."
        else
            cd output/build && make clean
        fi
        exit 0
        ;;
    d)
        rm -rf build
        rm -rf output/build
        rm -rf output/Debug
        rm -rf output/Release
        rm -rf output/staging
        exit 0
        ;;
    D)
        rm -rf build
        rm -rf output
        git submodule deinit .
        exit 0
        ;;
    ?)
        echo "    There is unrecognized parameter!"
        helpMessage
        exit 1
        ;;
    esac
done

if [ ! -n "$SOCPLAT" ] && [ ! -n "$IMAGE_NAME" ]; then
    SOCPLAT="x86_64"
elif [ ! -n "$SOCPLAT" ] && [ "$IMAGE_NAME" != "" ]; then
    SOCPLAT=$IMAGE_NAME
    toolchain="${CROSS_COMPILE_DIR}/$IMAGE_NAME.cmake"
fi
if [ ! -n "$BUILD_TYPE" ]; then
    BUILD_TYPE="Release"
fi
if [ ! -n "$BUILD_VER" ]; then
    BUILD_VER="0"
fi
if [ ! -n "$BUILD_PACK" ]; then
    BUILD_PACK=false
fi
if [ ! -n "$BUILD_DETAIL" ]; then
    BUILD_DETAIL=false
fi
if [ ! -d output/build ]; then
    mkdir -p output/build
fi
if [ ! -f "output/build/conan/conanbuildinfo.cmake" ]; then
    bash prepare.sh -t $BUILD_TYPE
fi

echo "##########################################################"
echo "soc: $SOCPLAT"
echo "type: $BUILD_TYPE"
echo "workspace: $CURDIR"
echo "toolchain: $toolchain"
echo "buildVersion: $BUILD_VER"
echo "buildPackage: $BUILD_PACK"
echo "##########################################################\n"

# 让命令执行过程可视化输出，用于此脚本调试
# set -v

git submodule update --init --recursive
cd output/build

if [ ! -n "$toolchain" ]; then
    cmake ../.. -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DBUILD_VERSION=$BUILD_VER -DTARGET_PLAT=$SOCPLAT
else
    cmake ../.. -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DBUILD_VERSION=$BUILD_VER -DTARGET_PLAT=$SOCPLAT -DCMAKE_TOOLCHAIN_FILE=$toolchain
fi
if $BUILD_DETAIL; then
    make -j20 VERBOSE=1
else
    make -j20
fi
if [ $? -ne 0 ]; then
    echo "build failure!!!"
    exit 1
fi
make install
conan info conanfile.py -g=graphDependencies.html
if $BUILD_PACK; then
    make pack
fi
