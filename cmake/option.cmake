unset(COMPONENT_VERSION)
unset(COMPONENT_VERSION_MAJOR)
unset(COMPONENT_VERSION_MINOR)
unset(COMPONENT_VERSION_PATCH)
unset(COMPONENT_VERSION_BUILD)
#############################################################################################
## Component version
############################################################################################
set(COMPONENT_VERSION_MAJOR 0)
set(COMPONENT_VERSION_MINOR 3)
set(COMPONENT_VERSION_PATCH 0)

if(NOT DEFINED BUILD_VERSION)
    set(COMPONENT_VERSION_BUILD 0)
else()
    set(COMPONENT_VERSION_BUILD ${BUILD_VERSION})
endif()

string(TIMESTAMP COMPONENT_BUILD_TIME "%Y-%m-%d %H:%M:%S")
set(COMPONENT_VERSION V${COMPONENT_VERSION_MAJOR}.${COMPONENT_VERSION_MINOR}.${COMPONENT_VERSION_PATCH}.${COMPONENT_VERSION_BUILD})
message(STATUS "${COMPONENT_NAME} time: ${COMPONENT_BUILD_TIME}")
message(STATUS "${COMPONENT_NAME} version: ${COMPONENT_VERSION}")

#############################################################################################
## configration file
#############################################################################################
set(IMAGE_TAG $ENV{IMAGE_TAG})
set(IMAGE_NAME $ENV{IMAGE_NAME})
configure_file(
    ${PROJECT_SOURCE_DIR}/cmake/version.yaml.in ${CMAKE_BINARY_DIR}/version.yaml
)
install(FILES ${CMAKE_BINARY_DIR}/version.yaml DESTINATION .)

configure_file(
    ${PROJECT_SOURCE_DIR}/cmake/deploy.sh.in ${CMAKE_BINARY_DIR}/deploy.sh
)

configure_file(
    ${PROJECT_SOURCE_DIR}/cmake/conanfile.py ${CMAKE_BINARY_DIR}/conanfile.py
)

#############################################################################################
## include/library directories, link library
#############################################################################################
include_directories(${CMAKE_BINARY_DIR})
include_directories(${PROJECT_SOURCE_DIR}/source)

#############################################################################################
## compile options : https://cmake.org/cmake/help/v3.10/command/add_compile_options.html
#############################################################################################

if(CMAKE_COMPILER_IS_GNUCC)
    add_compile_options(-Wall -Werror)
endif(CMAKE_COMPILER_IS_GNUCC)

#############################################################################################
configure_file(
    ${PROJECT_SOURCE_DIR}/cmake/config.h.in ${CMAKE_BINARY_DIR}/config.h
)
