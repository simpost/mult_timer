from conans import ConanFile, CMake, tools

class SimpCoreConan(ConanFile):
    name = "@COMPONENT_NAME@"
    version = "@COMPONENT_VERSION@"
    author = "@GIT_COMMIT_AUTHOR@(@GIT_COMMIT_EMAIL@)"
    topics = "(@CONAN_LIBS@)"
    license = "MIT License"
    settings = "os", "compiler", "build_type", "arch", "platform"
    url = "@GIT_REMOTE_URL@"
    description = "@CMAKE_BUILD_TYPE@ @COMPONENT_NAME@-@COMPONENT_VERSION@ on @TARGET_PLAT@(@GIT_BRANCH_NAME@, @GIT_COMMIT_HASH@)"
    generators = "cmake"

    def requirements(self):
        self.requires("utils/V0.1.0.0@components/stable")

    def package(self):
        self.copy("*")

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

    def configure(self):
        del self.settings.compiler.libcxx

